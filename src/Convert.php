<?php

namespace Converter;

use Imagick;

/**
 * Класс для работы конвертации pdf, pptx в изображение.
 *
 * @package     aziz/test-converter
 * @author      Aziz karimov <aziztwelve@gmail.com>
 * @version     1.0
 * @since       25.12.2020
 */
class Convert
{

    /** @var string */
    private static $file;
    /** @var string */
    private static $tempPath;
    /** @var string */
    private static $extension;
    /** @var string */
    private static $basename;

    /**
     * Конструктор
     * @access private Защищаем от создания через new
     */
    private function __construct(){}

    /**
     * Клонирование
     * @access private Защищаем от создания через clone
     */
    private function __clone() {}

    /**
     * Unserialize
     * @access private Защищаем от создания через unserialize
     */
    private function __wakeup() {}

    /**
     * Деструктор
     */
    public function __destruct() {}


    /**
     * @param string $filename
     *
     * @param string|null $outExt
     * @return array
     *
     * @throws ConvertException
     * @throws \ImagickException
     */
    public static function convertTo(string $filename, string $outExt = null): ?array
    {
        if ($outExt === 'pdf'){
            return self::convertFromPptxToPdf($filename, $outExt);
        }

        return self::convertFromPdfToImage($filename);
    }


    /**
     * @param string $filename
     *
     * @return array
     *
     * @throws ConvertException
     * @throws \ImagickException
     */
    public static function convertFromPdfToImage(string $filename): ?array
    {

        self::setup($filename);

        $pdf = new Imagick($filename);
        $noOfPagesInPDF = $pdf->getNumberImages();

        if ($noOfPagesInPDF) {

            $data = [];
            for ($i = 0; $i < $noOfPagesInPDF; $i++) {

                $url = $filename.'['.$i.']';
                $generateFileName = '/converted/'.($i+1).'-'.mt_rand().'.jpg';
                $savePath = self::$tempPath . $generateFileName;

                $image = new Imagick($url);

                $image->setImageFormat('jpg');
                $image->writeImage($savePath);

                $imageData = [
                    'out_path_file' => '/uploads'.$generateFileName,
                    'sort' => $i,
                    'default_file' => '/uploads/'.self::$basename
                ];

                $data[] = $imageData;
            }
            return $data;
        }
        return null;
    }

    /**
     * @param string $filename
     *
     * @param string $outExt
     * @return array
     *
     * @throws ConvertException
     */
    public static function convertFromPptxToPdf(string $filename, string $outExt): array
    {
        self::setup($filename);

        $outputExtension = self::$extension;
        $supportedExtensions = self::allowedExtension();

        if (!array_key_exists($outputExtension, $supportedExtensions)) {
            throw new ConvertException('Output extension({' .$outputExtension. '}) not supported for input file({' .self::$basename . '})');
        }
        $espFile = escapeshellarg(self::$file);
        $espOutputDirectory = escapeshellarg(self::$tempPath);

        $cmd = "sudo libreoffice --headless --convert-to {$outExt} {$espFile} --outdir {$espOutputDirectory}";

        $shell = self::execute($cmd);

        if (0 != $shell['return']) {
            throw new ConvertException('Convertion Failure! Contact Server Admin.');
        }

        $data = [
            'out_path_file' => '/uploads/'.pathinfo(self::$basename, PATHINFO_FILENAME).'.'.$outExt,
            'sort' => 0,
            'default_file' => '/uploads/'.self::$basename,

        ];
        return $data;
    }


    private static function execute($cmd, $input = ''): array
    {
        $process = proc_open($cmd, [0 => ['pipe', 'r'], 1 => ['pipe', 'w'], 2 => ['pipe', 'w']], $pipes);

        if (false === $process) {
            throw new ConvertException('Cannot obtain ressource for process to convert file');
        }

        fwrite($pipes[0], $input);
        fclose($pipes[0]);
        $stdout = stream_get_contents($pipes[1]);
        fclose($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        $rtn = proc_close($process);

        return [
            'stdout' => $stdout,
            'stderr' => $stderr,
            'return' => $rtn,
        ];

    }



    /**
     * @param string|null $extension
     *
     * @return array|mixed
     */
    private static function allowedExtension($extension = null): array
    {
        $allowedExt = [
            'pptx' => ['pdf'],
            'ppt' => ['pdf'],
            'pdf' => ['jpg'],
        ];

        if (null !== $extension) {
            if (isset($allowedExt[$extension])) {
                return $allowedExt[$extension];
            }

            return [];
        }
        return $allowedExt;
    }


    /**
     * @param $filename
     * @throws ConvertException
     */
    private static function setup($filename): void
    {
        self::$file = realpath($filename);
        self::$basename = pathinfo(self::$file, PATHINFO_BASENAME);
        $extension = pathinfo(self::$file, PATHINFO_EXTENSION);

        //Check for valid input file extension
        if (!array_key_exists($extension, self::allowedExtension())) {
            throw new ConvertException('Input file extension not supported -- '.$extension);
        }
        self::$extension = $extension;

        $tempPath = dirname(self::$file);
        self::$tempPath = realpath($tempPath);

    }
}