<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once '../vendor/autoload.php';

$pptxFile = './uploads/payform.pptx';
$pdfFile = './uploads/payform.pdf';
$saveAllPage = './uploads/converted/';

print_r(\Converter\Convert::convertTo($pdfFile));